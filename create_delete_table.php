<?php 
function create_table_for_sms_verify(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix."sms_verify";
 	if ($wpdb->get_var('SHOW TABLE LIKE '.$table_name) != $table_name) {
		$sql = "CREATE TABLE ". $table_name." ( 
            id mediumint(9) NOT NULL AUTO_INCREMENT, 
            mobile_number varchar(55) NOT NULL, 
            verification_code  varchar(55) NOT NULL, 
            verified tinyint(1) DEFAULT '0' NOT NULL , 
            PRIMARY KEY (id) 
            )". $charset_collate . ";";	
	
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
    }
}

function delete_table_for_sms_verify(){
    global $wpdb;
    $table_name = $wpdb->prefix."sms_verify";
    $query = "DROP TABLE IF EXISTS ".$table_name;
    $wpdb->query( $query );
}
