<?php
	
function my_menu_item_function(){
    global $wp_session;

    $sid = $_SESSION['sid'];
    $token = $_SESSION['token'];
    $activePhoneNumber = $_SESSION['activePhoneNumber'] ;


    // global $token;
    // global $activePhoneNumber;

    global $wpdb;
    $table_name = $wpdb->prefix."sms_verify";
    $sql = "SELECT * FROM  $table_name WHERE `verified`='1'";
    $all_data =  $wpdb->get_results( $sql);
    
    ?>
    <div class="container">
        <div class="row pt-4">
            <div class="col-md-10">
                <p>For this plugin you must <a href="https://www.twilio.com/try-twilio">sign up for your free Twilio trial account</a> </p>
                <p>When you signed up for your trial account, you verified your personal phone number. You can see your list of verified phone numbers on the <a href="https://www.twilio.com/console/phone-numbers/verified">Verified Caller IDs</a>  page.</p>
                <p>You must verify any non-Twilio phone numbers you wish to send SMS messages  to while in trial mode. This is an extra security measure for trial accounts that we remove once you upgrade your account. You may verify as many phone numbers as you like.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
            <?php 
            if ($sid=="" || $token=="" || $activePhoneNumber=="") {
 
                ?>
                <form  method="POST" action="" id="accountSidTokenForm">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter ACCOUNT SID" id="accountSid">
                        <div class="input-group-append">
                            <span class="input-group-text" >ACCOUNT SID</span>
                        </div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Enter AUTH TOKEN" id="accountToken">
                        <div class="input-group-append">
                            <span class="input-group-text" >AUTH TOKEN</span>
                        </div>
                    </div>

                    <div class="input-group mb-3">
                        <input type="text" class="form-control" placeholder="Active Phone Number" id="activePhoneNumber">
                        <div class="input-group-append">
                            <span class="input-group-text" >Active Phone Number</span>
                        </div>
                    </div>

                    <div class="input-group mb-3 d-flex justify-content-center">
                        <input type="submit" class="btn btn-primary" value="Send" >
                    </div>
                </form>
                <?php
            } else {
                ?>
                <p>You already enter ACCOUNT SID , AUTH TOKEN, Active Phone Number values</p>
                <?php

            } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <?php if($all_data): ?>
                    <h2>All verified numbers</h2>

                    <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Mobile number</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($all_data as $item): ?>
                            <tr>
                                <th scope="row"><?= $item->id ?></th>
                                <td><?= $item->mobile_number ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                    </table>
                <?php endif;?>
            </div>
        </div>

<script>

    jQuery("#accountSidTokenForm").on("submit", function( event ) {
        event.preventDefault();

    let accountSid = jQuery("#accountSid").val();
    let accountToken = jQuery("#accountToken").val();
    let activePhoneNumber = jQuery("#activePhoneNumber").val();

    jQuery.ajax({
      action:  'send_account_sid_token',
      type:    "POST",
      url:     ajax_object.ajaxurl,
      data:    {
        action: 'send_account_sid_token',
        accountSid: accountSid,
        accountToken: accountToken,
        activePhoneNumber: activePhoneNumber
        },
      success: function(data) {
        console.log(data);
        if (data) {
            jQuery("#accountSidTokenForm").remove()
        }
      }
    });


    })

</script>
    
    <?php
}
function build_my_menu_item(){
add_menu_page(
    'Sms Verify plugin settings',
    'Settings',
    'manage_options',
    'sms_verify',
    'my_menu_item_function',
    'dashicons-image-flip-horizontal'
);
}
add_action( 'admin_menu', 'build_my_menu_item');