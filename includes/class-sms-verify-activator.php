<?php

/**
 * Fired during plugin activation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Sms_Verify
 * @subpackage Sms_Verify/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Sms_Verify
 * @subpackage Sms_Verify/includes
 * @author     Armine Khachatryan <arminekhachatryan02@gmail.com>
 */
class Sms_Verify_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		

	}

}
