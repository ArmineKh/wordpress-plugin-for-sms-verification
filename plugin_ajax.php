<?php
include plugin_dir_path( __FILE__ ).'/vendor/autoload.php';
$sid = "";
$token = "";
$activePhoneNumber = "";
add_action( 'wp_ajax_send_account_sid_token', 'send_account_sid_token_callback' );
function send_account_sid_token_callback() {
    global $sid;
    global $token;
    global $activePhoneNumber;
    if(isset($_POST["accountSid"]) || isset($_POST["accountToken"]) || isset($_POST["activePhoneNumber"])){
        if(!empty($_POST["accountSid"]) || !empty($_POST["accountToken"]) || !empty($_POST["activePhoneNumber"])){

            // $sid = 'AC8029bb60795482898fb6a89b7a6bd8be';
            // $token = 'e28ab1957cf5717c1a61297c230821e1';
            $sid = $_POST["accountSid"];
            $token = $_POST["accountToken"];
            $activePhoneNumber = $_POST["activePhoneNumber"];

            if (!session_id()) {
                session_start();
                $_SESSION['sid'] = $sid;
                $_SESSION['token'] = $token;
                $_SESSION['activePhoneNumber'] = $activePhoneNumber;

            }
            echo $_SESSION['sid']." <br/>".$_SESSION['token']." <br/>".$_SESSION['activePhoneNumber'];


        }
    }

	wp_die(); 
}

add_action( 'wp_footer', 'send_mobile_number_action_javascript' ); 

function send_mobile_number_action_javascript() { ?>
	<script type="text/javascript" id="send_mobile_number_script">
	jQuery(document).ready(function($) {
        $('body').append(`<button type="button" class="btn btn-primary" data-toggle="modal" id="signupButton" data-target="#signupModal"> Sign up (sms-verify)</button>`);
    $('body').append(`<div class="modal fade" id="signupModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Sign up with sms-verify</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="mobileNumberSendForm"  method="POST" action="" ">
              <div class="input-group mb-3">
                  <input type="text" name="mobileNumber" class="form-control" placeholder="Enter Mobile Number" id="mobileNumber">
                  <div class="input-group-append">
                      <span class="input-group-text" >Mobile Number</span>
                  </div>
              </div>
              <div class="input-group mb-3 d-flex justify-content-center">
                <input type="submit" class="btn btn-primary" value="Send" >
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>`);

		
        $("#mobileNumberSendForm").on("submit", function( event ) {
            event.preventDefault();

        let mobileNumber = $("#mobileNumber").val();
        console.log(ajax_object.ajaxurl)

        $.ajax({
            action:  'send_mobile_number',
            type:    "POST",
            url:     ajax_object.ajaxurl,
            data:    {
            action: 'send_mobile_number',
            mobileNumber: mobileNumber
            },
            success: function(data) {
            console.log(data);
              if (data) {
                $(".modal-body #mobileNumberSendForm").remove()
                $(".modal-body").append(`<form  method="POST" action="" id="sendCodeForm">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" mname="verifyCode" placeholder="Enter Verify Code" id="verifyCode">
                    <div class="input-group-append">
                        <span class="input-group-text" >SMS is sent to Your Mobile Number</span>
                    </div>
                </div>
                <div class="input-group mb-3 d-flex justify-content-center">
                  <input type="submit" class="btn btn-primary" value="Send" >
                </div>
            </form>`)
              }
            }
        });

        });
	});
	</script> <?php
}



add_action( 'wp_ajax_send_mobile_number', 'send_mobile_number_callback' );

function send_mobile_number_callback() {
	global $wpdb;
    global $wp_session;

    $sid = $_SESSION['sid'] ;
    $token = $_SESSION['token'] ;
    $activePhoneNumber = $_SESSION['activePhoneNumber'] ;

    $mobile_number = $_POST["mobileNumber"];
    $rand_no = rand(10000, 99999);

    $twilio_number = $activePhoneNumber;

    $client = new Twilio\Rest\Client($sid, $token);
    $client->messages->create(
        // Where to send a text message
        $mobile_number,
        array(
            'from' => $twilio_number,
            'body' => $rand_no
        )
    );
    if (!session_id()) {
        session_start();
        $_SESSION['mobile_number'] = $mobile_number;
    }
    $table_name = $wpdb->prefix."sms_verify";
    $wpdb->query("INSERT INTO $table_name( mobile_number, verification_code) VALUES('$mobile_number', '$rand_no')");

	wp_die(); 
}




add_action( 'wp_footer', 'send_code_action_javascript' ); 

function send_code_action_javascript() { ?>
	<script type="text/javascript">
	jQuery(document).ready(function($) {

		
        $("#sendCodeForm").on("submit", function( event ) {
            event.preventDefault();

        let verifyCode = $("#verifyCode").val();

        $.ajax({
            action:  'send_verify_code',
            type:    "POST",
            url:     ajax_object.ajaxurl,
            data:    {
            action: 'send_verify_code',
            verifyCode: verifyCode
            },
            success: function(data) {
            console.log(data);
              if (data) {
                $(".modal-body #sendCodeForm").remove()
                $(".modal-body").append(`<p>You are sign in</p>`);
              }
            }
        });

        });
	});
	</script> <?php
}



add_action( 'wp_ajax_send_verify_code', 'send_verify_code_callback' );

function send_verify_code_callback() {
	global $wpdb;
    global $wp_session;

    $mobile_number = $_SESSION['mobile_number'] ;
    $verify_code = $_POST["verifyCode"];

    $table_name = $wpdb->prefix."sms_verify";


    $result = $wpdb->get_results ( "FROM  $table_name WHERE mobile_number='$mobile_number' and verification_code='$verify_code'" );

    if($result){

        $wpdb->query("UPDATE $table_name SET verified='1' WHERE mobile_number='$mobile_number' and verification_code='$verify_code'");
        
    }

	wp_die(); 
}
