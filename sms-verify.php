<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             1.0.0
 * @package           Sms_Verify
 *
 * @wordpress-plugin
 * Plugin Name:       Sms-verify
 * Plugin URI:        #
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Armine Khachatryan
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sms-verify
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SMS_VERIFY_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sms-verify-activator.php
 */
function activate_sms_verify() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sms-verify-activator.php';
	Sms_Verify_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sms-verify-deactivator.php
 */
function deactivate_sms_verify() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sms-verify-deactivator.php';
	Sms_Verify_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sms_verify' );
register_deactivation_hook( __FILE__, 'deactivate_sms_verify' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sms-verify.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sms_verify() {

	$plugin = new Sms_Verify();
	$plugin->run();

}
run_sms_verify();


require_once plugin_dir_path( __FILE__ ) . 'create_delete_table.php';

register_activation_hook( __FILE__, 'create_table_for_sms_verify' );
register_deactivation_hook( __File__, 'delete_table_for_sms_verify');

require_once plugin_dir_path( __FILE__ ) . 'create_menu_item.php';
require_once plugin_dir_path( __FILE__ ) . 'plugin_ajax.php';





add_action( 'admin_enqueue_scripts', function(){

	wp_enqueue_style( 'bootstrap_css',"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" );

	wp_enqueue_script( 'bootstrap_js', "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js", array( 'jquery' ), '1.0', true );

	wp_localize_script( 'bootstrap_js', 'ajax_object',
            array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

}, 99 );

add_action( 'wp_enqueue_scripts', function(){
	wp_enqueue_style( 'bootstrap_css',"https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" );

	wp_enqueue_script( 'bootstrap_js', "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js", array( 'jquery' ), '1.0', true );

	wp_enqueue_style( 'custom_css',plugin_dir_url( __FILE__ )."css/custom.css" );

	wp_enqueue_script( 'custom_js', plugin_dir_url( __FILE__ )."js/custom.js", array( 'jquery' ), '1.0', true );

	wp_localize_script( 'custom_js', 'ajax_object',
            array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

} , 99);

function register_session(){
    if( !session_id() )
        session_start();
}
add_action('init','register_session');